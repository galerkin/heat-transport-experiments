% Code from Qiqi Wang, MIT 2.097/6.339/16.920: Num. Methods for PDEs,
% Fall 2017
% See https://youtu.be/rSOmdsyEBTE for live coding / demo


%N = 10; % N intervals (I think)
N = 1000;
x = linspace(0,1,N+1); % N+1 very important
dx = x(2) - x(1);

global k A b;

A = eye(N-1);
A = A * (-2 / dx^2);
A = A + 1/dx^2 * diag(ones(N-2,1),1);
A = A + 1/dx^2 * diag(ones(N-2,1),-1);

b = zeros(N-1,1);
k = 0.01; % to make it slow, so we can see things
u = exp(-(x - 0.5).^2 / 0.1^2); % initial condition
u0 = u(2:end-1); % should only be values on the interior
tic;
[t, u] = ode45(@ddt_heat, [0,1], u0);
toc;

hold off;
plot(x(2:end-1)',u0);
hold on;
plot(x(2:end-1)',u(end,:)')