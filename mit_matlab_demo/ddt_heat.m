function [ dudut ] = ddt_heat( t, u ) % ODE45 expects t first
    global k A b
    dudut = k * (A * u + b);
end

